<?php
require "class-basket.php";
$basket = new Basket();
if ( isset( $_GET['action'] ) && ! empty( $_GET['action'] ) ) {
	switch ( $_GET['action'] ) {
		case 'remove':
			$pid = intval( $_GET['pid'] );
			$basket->remove( $pid );
			break;
	}
}
$basketItems = Basket::getItems();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>لیست محصولات</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <style>
        body {
            padding: 50px 0;
            direction: rtl;
        }
    </style>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">نمایش سبد خرید</div>
                <div class="panel-body">
                    <div class="row">
                        <table class="table table-bordered table-hover table-stripe">\
                            <tr>
                                <th>نام محصول</th>
                                <th>قیمت فی</th>
                                <th>تعداد</th>
                                <th>قیمت کل</th>
                                <th>عملیات</th>
                            </tr>
							<?php foreach ( $basketItems as $product_id => $item ) : ?>
                                <tr class="<?php echo empty( $record['reply'] ) ? 'danger' : 'success'; ?>">
                                    <td><?php echo $item['title']; ?></td>
                                    <td><?php echo $item['price'] ?></td>
                                    <td><?php echo $item['count']; ?></td>
                                    <td><?php echo $item['price'] * $item['count']; ?></td>
                                    <td><a href="?action=remove&pid=<?php echo $product_id; ?>"></a></td>
                                </tr>
							<?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>

