<?php
require "functions.php";
$date = null;
if (isset($_GET['dateFilter']) && !empty($_GET['dateFilter'])) {
    $date = $_GET['dateFilter'];
}
$list = getData($date);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>نتیجه ثبت درخواست</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <style>
        body {
            padding: 50px 0;
            direction: rtl;
        }
    </style>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">لیست درخواست ها</div>
                <div class="panel-body">
                    <form action="" method="get">
                        <input type="date" name="dateFilter">
                        <button type="submit" name="submitFilter">فیلتر بر اساس تاریخ</button>
                    </form>
                    <table class="table table-bordered table-hover table-stripe">\
                        <tr>
                            <th>شناسه درخواست</th>
                            <th>درخواست کننده</th>
                            <th>شماره تماس</th>
                            <th>متن درخواست</th>
                            <th>تاریخ درخواست</th>
                            <th>کد رهگیری</th>
                            <th>عملیات</th>
                        </tr>
                        <?php while ($record = $list->fetch(PDO::FETCH_ASSOC)): ?>
                            <tr class="<?php echo empty($record['reply']) ? 'danger' : 'success'; ?>">
                                <td><?php echo $record['id']; ?></td>
                                <td><?php echo !empty($record['full_name']) ? $record['full_name'] : $record['first_name'] . ' ' . $record['last_name']; ?></td>
                                <td><?php echo $record['mobile']; ?></td>
                                <td><?php echo $record['content']; ?></td>
                                <td><?php echo $record['created_at']; ?></td>
                                <td><?php echo $record['code']; ?></td>
                                <td>
                                    <a href="reply.php?id=<?php echo $record['id']; ?>">ثبت پاسخ</a>
                                </td>
                            </tr>
                        <?php endwhile; ?>
                    </table>


                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>

